package datetime

import java.time.{Clock, Instant, ZoneId}
import org.scalatest._

class UtilsSpec extends FlatSpec with Matchers {

  import UtilsSpec._

  val utilsPST = new Utils(Clock.fixed(Instant.now, ZoneId.of("America/Los_Angeles")))
  val utilsMST = new Utils(Clock.fixed(Instant.now, ZoneId.of("-07:00")))
  val utilsCST = new Utils(Clock.fixed(Instant.now, ZoneId.of("America/Chicago")))

  "oneHourEarlier" should "correctly calculate one hour earlier" in {

    utilsPST.oneHourEarlier("2015-01-07 2:29PM") shouldBe "2015-01-07 1:29PM"

  }

  it should "evaluate to the previous day if just after midnight" in {

    utilsPST.oneHourEarlier("2015-01-01 12:34AM") shouldBe "2014-12-31 11:34PM"

  }

  "isCentralTime" should "evaluate to true if device operating in CST" in {

    assert(utilsCST.isCentralTime)

  }

  it should "evaluate to false if not" in {

    assert(!utilsPST.isCentralTime)

  }

  val sixPM = "2014-02-08 6:00PM"

  "toCalendarFormat" should "evaluate to the equivalent time in ISO format" in {

    utilsPST.toCalendarFormat(sixPM) shouldBe "2014-02-09T02:00:00.000Z"

  }

  it should "take into account device time zone" in {

    utilsMST.toCalendarFormat(sixPM) shouldBe "2014-02-09T01:00:00.000Z"

  }

  it should "work for CST as well" in {

    utilsCST.toCalendarFormat(sixPM) shouldBe "2014-02-09T00:00:00.000Z"

  }

  "getEventTimestamp" should "evaluate to an ISO timestamp" in {

    utilsPST.getEventTimestamp(sixPM) shouldBe "2014-02-09T02:00:00.000Z"

  }

  it should "evaluate to an iso timestamp AN HOUR EARLIER if CST" in {

    utilsCST.getEventTimestamp(sixPM) shouldBe "2014-02-08T23:00:00.000Z"

  }

  "filterAds function" should "partition ads whose start time is earlier than user current time" in {

    utilsCST.filterAds(request1, ads) shouldBe "banana"

  }

  it should "also apply the second predicate concerning falling outside of scheduled show times" in {
    utilsCST.filterAds(request2, ads) shouldBe "banana"
  }

}

object UtilsSpec {

  val ads = Seq(
    Ad(1,"ad_01","2020-04-01T02:00:00.000Z"),
    Ad(2,"ad_02","2020-04-02T02:00:00.000Z"),
    Ad(3,"ad_03","2020-04-03T02:00:00.000Z"),
    Ad(4,"ad_04","2020-04-04T02:00:00.000Z"),
    Ad(5,"ad_05","2020-04-01T11:00:00.000Z"),
    Ad(6,"ad_06","2020-04-01T13:00:00.000Z"),
    Ad(7,"ad_07","2020-04-01T10:00:00.000Z"),
    Ad(8,"ad_08","2020-04-02T18:30:00.000Z"),
    Ad(9,"ad_09","2020-04-01T15:30:00.000Z"),
    Ad(10,"ad_10","2020-04-02T14:00:00.000Z"),
    Ad(11,"ad_11","2020-04-02T19:30:00.000Z"),
    Ad(12,"ad_12","2020-04-02T07:30:00.000Z"),
    Ad(13,"ad_13","2020-04-02T20:00:00.000Z"),
    Ad(14,"ad_14","2020-04-02T14:30:00.000Z"),
    Ad(15,"ad_15","2020-04-03T02:30:00.000Z"),
    Ad(16,"ad_16","2020-04-04T00:30:00.000Z"),
    Ad(17,"ad_17","2020-04-03T17:00:00.000Z"),
    Ad(18,"ad_18","2020-04-04T19:30:00.000Z"),
    Ad(19,"ad_19","2020-04-02T20:00:00.000Z"),
    Ad(20,"ad_20","2020-04-02T10:00:00.000Z"),
    Ad(21,"ad_21","2020-04-03T03:30:00.000Z"),
    Ad(22,"ad_22","2020-04-02T19:30:00.000Z"),
    Ad(23,"ad_23","2020-04-02T21:30:00.000Z"),
    Ad(24,"ad_24","2020-04-02T04:00:00.000Z"),
    Ad(25,"ad_25","2020-04-02T11:30:00.000Z"))

  val request1 = Request("2020-04-01 5:30AM",
                         "America/Los_Angeles",
                         Seq.empty)

  val request2 = request1.copy(
    scheduledShows = Seq(
      ScheduledShow("show_1", "2020-04-02 7:00AM", 15),
      ScheduledShow("show_2", "2020-04-02 12:00PM", 120),
      ScheduledShow("show_3", "2020-04-02 6:30PM", 150),
      ScheduledShow("show_4", "2020-04-03 4:00PM", 120),
      ScheduledShow("show_5", "2020-04-04 5:00AM", 60),
      ScheduledShow("show_6", "2020-04-04 7:00AM", 90)))

}
