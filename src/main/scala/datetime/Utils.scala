package datetime

import java.time.format.DateTimeFormatter
import java.time.{Clock, LocalDateTime, ZoneId, ZonedDateTime, Instant}

class Utils(systemClock: Clock) {

  private val clock = systemClock

  private val basicFormatPattern = "yyyy-MM-dd h:mma"
  private val basicFormatFormatter = DateTimeFormatter.ofPattern(basicFormatPattern)

  private val isoFormatPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
  private val isoFormatFormatter = DateTimeFormatter.ofPattern(isoFormatPattern)

  private val centralTimeZone = "America/Chicago"

  /*
   * Accepts a date-time string assumed to be in BASIC_FORMAT.
   * Returns a BASIC_FORMAT date-time string representing one hour earlier.
   */
  def oneHourEarlier(dateTime: String): String = {
    val localDateTime = LocalDateTime.from(basicFormatFormatter.parse(dateTime))
    localDateTime.minusHours(1).format(basicFormatFormatter)
  }

  // Returns true if the computer is operating in the Central time zone.
   def isCentralTime: Boolean = {
     clock.getZone == ZoneId.of(centralTimeZone)
   }

  /*
   * Accepts a date-time string in BASIC_FORMAT and returns a version of the
   * same time in Greenwich Mean Time (GMT) in ISO_FORMAT. The result will vary
   * based on the time zone the computer is operating in. For reference note
   * that GMT is 8 hours ahead of PST and 5 hours ahead of EST.
   */
  def toCalendarFormat(dateTime: String): String = {
    val localDateTime = LocalDateTime.from(basicFormatFormatter.parse(dateTime))
    val zonedDateTime = ZonedDateTime.of(localDateTime, clock.getZone)
    val sameInstantInUTC = zonedDateTime.withZoneSameInstant(ZoneId.of("UTC"))
    sameInstantInUTC.format(isoFormatFormatter)
  }

  /*
   * Accepts a date-time string in BASIC_FORMAT and returns a string in
   * ISO_FORMAT. If the device time zone is Central, returns the timestamp of
   * the time one hour earlier than the passed-in date-time string; otherwise
   * returns the timestamp of the passed-in date-time string, unadjusted.
   */
  def getEventTimestamp(dateTime: String): String = {
    toCalendarFormat(if (isCentralTime) oneHourEarlier(dateTime) else dateTime)
  }

  case class ParsedAd(id: Int, name: String, startTime: Instant)

  object ParsedAd {
    def fromAd(ad: Ad): ParsedAd = {
      val ldt = LocalDateTime.from(isoFormatFormatter.parse(ad.startTimeInUTC))
      val zdt = ZonedDateTime.of(ldt, ZoneId.of("UTC"))
      val startTime = zdt.toInstant
      ParsedAd(id = ad.id, name = ad.name, startTime = startTime)
    }
  }

  case class ParsedScheduledShow(name: String, startTime: Instant, lengthInMinutes: Int)

  object ParsedScheduledShow {
    def fromScheduledShow(zoneId: ZoneId, show: ScheduledShow): ParsedScheduledShow = {
      val ldt = LocalDateTime.from(basicFormatFormatter.parse(show.startTime))
      val zdt = ZonedDateTime.of(ldt, zoneId)
      val startTime = zdt.toInstant
      ParsedScheduledShow(name = show.name, startTime = startTime, lengthInMinutes = show.lengthInMinutes)
    }
  }

  private def yetToStart(currentTime: Instant, ad: ParsedAd): Boolean =
    ad.startTime isAfter currentTime

  private def fallsOutsideOfScheduledShows(adTime: Instant,
                                           scheduledShows: Seq[ParsedScheduledShow]) = {
    scheduledShows.forall(
      show => {
        val showEndTime = show.startTime.plusSeconds(show.lengthInMinutes * 60)
        adTime.isBefore(show.startTime) || adTime.isAfter(showEndTime)
      })
  }

  // return all ads whose start time is after current time (relative to user)
  // return all ads whose start time is outside of scheduled show time
  def filterAds(request: Request, ads: Seq[Ad]): (Seq[ParsedAd], Seq[ParsedAd]) = {
    val localDateTime = LocalDateTime.from(basicFormatFormatter.parse(request.currentTime))
    val userTimeZone = ZoneId.of(request.timeZone)
    val userCurrentTime = ZonedDateTime.of(localDateTime, userTimeZone).toInstant

    val parsedShows = request.scheduledShows.map(ParsedScheduledShow.fromScheduledShow(userTimeZone, _))

    val filteredAds = ads
      .map(ParsedAd.fromAd _)
      .partition(
        parsedAd => yetToStart(userCurrentTime, parsedAd) &&
        fallsOutsideOfScheduledShows(parsedAd.startTime, parsedShows))

    filteredAds
  }

}

case class ScheduledShow(name: String,
                         startTime: String,
                         lengthInMinutes: Int)

case class Request(currentTime: String,
                   timeZone: String,
                   scheduledShows: Seq[ScheduledShow])

case class Ad(id: Int, name: String, startTimeInUTC: String)
